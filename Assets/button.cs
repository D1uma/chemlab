﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class button : MonoBehaviour {
    public ParticleSystem water;
    public GameObject dish;
  

	// Use this for initialization
	void Start () {
        water.Pause(true);
        water.Clear(true);
        dish.GetComponent<Renderer>().sharedMaterial.color = Color.cyan;
        this.GetComponent<Renderer>().sharedMaterial.color = Color.yellow;
      //  Debug.logger.Log("Frames per second", (1.0f / Time.deltaTime));
    }
	
	// Update is called once per frame
	void Update () {
      
        if (Input.GetKeyDown(KeyCode.A))
        {
            water.Play(true);
            dish.GetComponent<Renderer>().sharedMaterial.color =Color.blue;
        }
        else if (Input.GetKeyDown(KeyCode.Z))
        {
            water.Pause(true);
            water.Clear(true);

        }
        
    }
   
}